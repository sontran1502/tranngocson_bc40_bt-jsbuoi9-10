function layDuLieu() {
    const _tk = document.getElementById('tknv').value;
    const _ten = document.getElementById('name').value;
    const _email = document.getElementById('email').value;
    const _pass = document.getElementById('password').value;
    const _ngayLv = document.getElementById('datepicker').value;
    const _luongCb = document.getElementById('luongCB').value;
    const _chucVu = document.getElementById('chucvu').value;
    const _gioLam = document.getElementById('gioLam').value * 1;

    var nv = new nvModel(_tk, _ten, _email, _pass, _ngayLv, _luongCb, _chucVu, _gioLam);
    return nv;

}

function renderDsnv(arr) {
    var content = '';
    for (var i = 0; i < arr.length; i++) {
        var item = arr[i];
        var time = item.giolam;
        var danhHieu = '';

        if (0 < time && time < 160) {
            danhHieu = "nhân viên Trung Bình";

        } else if (160 <= time && time < 176) {
            danhHieu = "nhân viên khá";

        } else if (176 <= time && time < 192) {
            danhHieu = "nhân viên Giỏi";

        } else if (192 <= time) {
            danhHieu = "nhân viên Xuất Sắc"
        } else {
        }

        var tongLuong = 0;
        if (item.chucVu === "Sếp") {
            tongLuong = item.luongCb * 3;
        } else if (item.chucVu === "Trưởng phòng") {
            tongLuong = item.luongCb * 2;
        } else if (item.chucVu === "Nhân viên") {
            tongLuong = item.luongCb;
        } else {
        }

        var contenTr = `
        <tr>
        <td>${item.tk}</td>
        <td>${item.ten}</td>
        <td>${item.email}</td>
        <td>${item.ngayLv}</td>
        <td>${item.chucVu}</td>
        <td>${tongLuong.toLocaleString({})}</td>
        <td>${danhHieu}</td>
        <td><button onclick="xoaNv('${item.tk}')" class="btn btn-danger">Xóa</button></td>
        <td> <button onclick="suaNv('${item.tk}')" data-toggle="modal" data-target="#myModal"  class="btn btn-warning">sửa</button></td>
        </tr>`
        content += contenTr;
    }
    document.getElementById("tableDanhSach").innerHTML = `${content}`
}

function vitri(id, arr) {
    var index = -1;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i].tk == id) {
            index = i;
        }
    }
    return index;
}

function showDuLieu(nv) {
    document.getElementById('tknv').value=nv.tk;
    document.getElementById('name').value=nv.ten;
    document.getElementById('email').value=nv.email;
    document.getElementById('password').value=nv.pass;
    document.getElementById('datepicker').value=nv.ngayLv;
    document.getElementById('luongCB').value=nv.luongCb;
    document.getElementById('chucvu').value=nv.chucVu;
    document.getElementById('gioLam').value=nv.giolam;
}