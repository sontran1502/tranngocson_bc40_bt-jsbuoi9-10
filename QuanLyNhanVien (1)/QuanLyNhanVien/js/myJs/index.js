var dsnv = [];

var dsnvJson = localStorage.getItem("DSNV");
if (dsnvJson != null) {
    dsnv = JSON.parse(dsnvJson);
    renderDsnv(dsnv);
}


// thêm 
document.getElementById("btnThemNV").onclick = function () {
    var nv = layDuLieu();
    var isValidate = true;
    isValidate = ktKyTuMk(nv.pass) && ktDodaiMK(nv.pass);
    isValidate = isValidate &
        ktDodai(nv.tk, 4, 6) &
        ktTenlaChu(nv.ten) &
        ktEmail(nv.email) &
        ktNgayThang(nv.ngayLv) &
        ktLuongCb(nv.luongCb) &
        ktGioLv(nv.giolam) &
        ktChucVu(nv.chucVu);


    if (isValidate) {
        dsnv.push(nv);
        var dsnvJson = JSON.stringify(dsnv);
        localStorage.setItem("DSNV", dsnvJson);
        renderDsnv(dsnv);
    }

}

// xóa
function xoaNv(id) {
    var index = vitri(id, dsnv);
    if (index != -1) {
        dsnv.splice(index, 1);
        renderDsnv(dsnv);
    }
    var dsnvJson = JSON.stringify(dsnv);
    localStorage.setItem("DSNV", dsnvJson);
    renderDsnv(dsnv);

}

// sửa
function suaNv(id) {
    var index = vitri(id, dsnv);
    if (index == -1) {
        return;
    }
    var nv = dsnv[index];
    showDuLieu(nv);
}
// cập nhật
document.getElementById("btnCapNhat").onclick = function () {
    var nv = layDuLieu();
    var isValidate = true;
    isValidate = ktKyTuMk(nv.pass) && ktDodaiMK(nv.pass);
    isValidate = isValidate &
        ktDodai(nv.tk, 4, 6) &
        ktTenlaChu(nv.ten) &
        ktEmail(nv.email) &
        ktNgayThang(nv.ngayLv) &
        ktLuongCb(nv.luongCb) &
        ktGioLv(nv.giolam) &
        ktChucVu(nv.chucVu);


    if (isValidate) {
        var index = vitri(nv.tk, dsnv);
        if (index != -1) {
            dsnv[index] = nv;
            renderDsnv(dsnv);
        }
        var dsnvJson = JSON.stringify(dsnv);
        localStorage.setItem("DSNV", dsnvJson);
        renderDsnv(dsnv);
    }
}



