function ktDodai(value,min,max){
    var length = value.length;

    if(length<min|| max< length){
        document.getElementById("tbTKNV").innerText="Tài Khoản Phải Chứa Từ 4 - 6 Ký tự, Không Được để trống"
        return false;
    }else{
        document.getElementById("tbTKNV").innerText ="";
        return true;
        
    }
}

function ktTrung(value,arr){
    var index = arr.findIndex(function(item){
        return value == item.tk;
    })
    if(index==-1){
        document.getElementById("tbTKNV").innerText ="";
        return true;
    }else{
        document.getElementById("tbTKNV").innerText ="Mã Nhân Viên Đã Tồn Tại";
        return false;
    }
}

function ktChoTrong(value,idErr){
    var reg = /^\S/;
    var isSpace = reg.test(value);
    if(isSpace){
        document.getElementById(idErr).innerText ="";
        return true;
    }else{
        document.getElementById(idErr).innerText =" Không Được để trống";
        return false;
    }
}
// ten
function ktTenlaChu(value){
    var reg = /\b[^\d\W]+\b/g;
    var isTen = reg.test(value);
    if(isTen){
        document.getElementById("tbTen").innerText ="";
        return true;
    }else{
        document.getElementById("tbTen").innerText =" Tên phải là chữ Không Được để trống";
        return false;
    }
}

// email

function ktEmail(value){
    const reg =
  /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var isEmail = reg.test(value);
  if(isEmail){
    document.getElementById("tbEmail").innerText="";
    return true
  }else{
    document.getElementById("tbEmail").innerText=`Email Không Hợp Lệ, Không Được để trống`;
        return false;
  }
}

// password
function ktDodaiMK(value){
    var length = value.length;
    if(6<= length && length<= 10){
        document.getElementById("tbMatKhau").innerText ="";
        return true;
    }else{
        document.getElementById("tbMatKhau").innerText="Mật Khẩu Phải Chứa Từ  6 - 10 Ký tự, Không Được để trống"
        return false;
    }
}
function ktKyTuMk(value){
    var reg = /^(?=\D*\d)(?=.*?[a-zA-Z]).*[\W_].*$/;
    var isPass= reg.test(value);
    if(isPass){
        document.getElementById("tbMatKhau").innerText ="";
        return true;
    }else{
        document.getElementById("tbMatKhau").innerText="Mật Khẩu chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt"
        return false;
    }
} 
// Định dạng mm/dd/yyyy
function ktNgayThang(value){
    var reg =/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/ ;
    var isdate = reg.test(value);
    if(isdate){
        document.getElementById("tbNgay").innerText ="";
        return true;
    }else{
        document.getElementById("tbNgay").innerText ="Ngày làm không để trống, định dạng mm/dd/yyyy";
        return false;
    }
}
// luongCB
function ktLuongCb(value){
    if(1e6<=value && value<=20e6){
        document.getElementById("tbLuongCB").innerText="";
         return true
        
    }else{
        document.getElementById("tbLuongCB").innerText=`Lương cơ bản 1 000 000 - 20 000 000, không để trống`;
        return false;
    }
}
// gioLam
function ktGioLv(value){
    if(80<=value && value<= 200){
        document.getElementById("tbGiolam").innerText="";
         return true
    }else{
        document.getElementById("tbGiolam").innerText=`Số giờ làm trong tháng 80 - 200 giờ, không để trống `;
        return false;
    }
}
// chức vụ 

function ktChucVu(value){
    if(value=="Chọn chức vụ"){
        document.getElementById("tbChucVu").innerText="Chức vụ phải chọn chức vụ hợp lệ (Giám đốc, Trưởng Phòng, Nhân Viên)";
        return false;
    }else{
        document.getElementById("tbChucVu").innerText="";
        return true;
    }
}